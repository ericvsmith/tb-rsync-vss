////////////////////////////////////////////////////////////////////////
// Create a VSS snapshot, map a drive, and execute a command while
//  the snapshot exists. Delete the snapshot on exit.
//
// Copyright: 2009-2015, True Blade Systems, Inc.  All rights reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Notes:
//  Loosely based on vshadow from the VSS SDK.
//  Needs better error logging when an ATL exception is caught.
//  See http://cygwin.com/cygwin-ug-net/ntsec.html#ntsec-setuid-overview
//   for how to set up cygwin sshd.
////////////////////////////////////////////////////////////////////////

/***
Sample command lines:

a b c "/|tmp-drive=x|rsync=c:/opt/cygwin/bin/rsync.exe|cygpath=true|rsync-arg-0=--foo|logfile=c:\home\eric\tb-rsync-vss.log|verbose=true|rsync-arg-1=bar|c:/Users/"

Starts cmd.exe, taking a snapshot of c: and making it available as x:
Note that it starts cmd.exe with a parameter of x:/, which isn't the best
way to start cmd.exe (although as of now it has no bad side effects).
/|tmp-drive=x|rsync=c:\windows\system32\cmd.exe|cygpath=false|verbose=true|c:/

***/

#include <SDKDDKVer.h>

// allow us to use fopen without warnings
#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>

// ATL is used for COM wrappers (CComPtr)
#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS      // some CString constructors will be explicit
#include <atlbase.h>

// VSS is the Volume Shadow Copy service
#include "vss.h"
#include "vswriter.h"
#include "vsbackup.h"

#include <string>
#include <vector>
#include <map>
#include <fstream>

#include <boost/algorithm/string.hpp>
//#include <boost/algorithm/string/replace.hpp>
#include <boost/format.hpp>

class Logger {
public:
    Logger() {m_verbose = false;}
    void log(const std::wstring& msg);
    void log(const boost::basic_format<wchar_t>& msg) {this->log(msg.str());}
    void debug(const std::wstring& msg);
    void debug(const boost::basic_format<wchar_t>& msg) {this->debug(msg.str());}
    void set_filename(const std::wstring& filename) {this->m_filename = filename;}
    void set_verbose(bool verbose) {this->m_verbose = verbose;}
private:
    std::wstring m_filename;
    bool m_verbose;
};

static Logger logger;

#include <ctime>

void
Logger::log(const std::wstring& msg)
{
    // asctime is not the prettiest timestamp in the world, but it's easy to get
    std::time_t t;
    std::time(&t);
    char* time_str = std::asctime(std::localtime(&t));
    time_str[24] = ' ';  // convert the newline to a space

    fprintf(stderr, "%s%ls\n", time_str, msg.c_str());
    fflush(stderr);

    if (this->m_filename.length() != 0) {
        // opening the file every time is not very efficient, but it
        //  does ensure everything is written out
        std::wofstream fl;
        fl.open(this->m_filename, std::ofstream::out | std::ofstream::app);
        fl << time_str << msg << std::endl;
        fl.close();
    }
}

void
Logger::debug(const std::wstring& msg)
{
    if (this->m_verbose) {
        this->log(msg);
    }
}

class CError {
public:
    CError(HRESULT hr, wchar_t* str, int line=-1) : m_hr(hr), m_str(str), m_line(line) {}
    CError(HRESULT hr, const std::wstring& str, int line=-1) : m_hr(hr), m_str(str), m_line(line) {}
    CError(HRESULT hr, const boost::basic_format<wchar_t>& str, int line=-1) : m_hr(hr), m_str(str.str()), m_line(line) {}
    HRESULT m_hr;
    std::wstring m_str;
    int m_line;
};

static void inline
checked_call(HRESULT hr, wchar_t* msg, int line)
{
    if FAILED(hr) {
        throw CError(hr, msg, line);
    }
}

static void inline
checked_win32_call(BOOL res, wchar_t* msg, int line)
{
    if (!res) {
        DWORD dwLastError = GetLastError();
        HRESULT hrInternal = HRESULT_FROM_WIN32(dwLastError);
        if (dwLastError != NOERROR) {
            throw CError(hrInternal, msg, line);
        }
    }
}

#define CHECKED_CALL(result, msg) logger.debug(boost::wformat(L"calling %s %d") % msg % __LINE__); checked_call(result, msg, __LINE__); logger.debug(L"done")
#define CHECKED_WIN32_CALL(result, msg) logger.debug(boost::wformat(L"calling %s %d") % msg % __LINE__); checked_win32_call(result, msg, __LINE__); logger.debug(L"done")

class ComInitialize {
public:
    ComInitialize() {
        // Initialize COM
        CHECKED_CALL(CoInitialize(NULL), L"CoInitialize");
    }
    ~ComInitialize() {
        CoUninitialize();
    }
};


class ComSecurity {
public:
    ComSecurity() {
        CHECKED_CALL(CoInitializeSecurity(
                     NULL,                           //  Allow *all* VSS writers to communicate back!
                     -1,                             //  Default COM authentication service
                     NULL,                           //  Default COM authorization service
                     NULL,                           //  reserved parameter
                     RPC_C_AUTHN_LEVEL_PKT_PRIVACY,  //  Strongest COM authentication level
                     RPC_C_IMP_LEVEL_IDENTIFY,       //  Minimal impersonation abilities
                     NULL,                           //  Default COM authentication settings
                     EOAC_NONE,                      //  No special options
                     NULL                            //  Reserved parameter
                     ), L"CoInitializeSecurity");
    }
};


// Removes the mapping in the destructor
class DriveMapper {
public:
    DriveMapper(const std::wstring& device_name, wchar_t map_to_drive_letter) :
      m_device_name(device_name), m_map_to_drive(&map_to_drive_letter, 1) {
        // map a drive letter to this volume
        m_map_to_drive += L":";

        logger.log(boost::wformat(L"mapping %s to %s") % m_device_name % m_map_to_drive);
        CHECKED_WIN32_CALL(DefineDosDevice(0, m_map_to_drive.c_str(), m_device_name.c_str()), L"DefineDosDevice");
    }
    ~DriveMapper() {
        // Unmap the drive letter.
        // Don't check the result, just call it. We can't do anything
        //  if it fails.
        DefineDosDevice(DDD_REMOVE_DEFINITION|DDD_EXACT_MATCH_ON_REMOVE,
                        m_map_to_drive.c_str(), m_device_name.c_str());
    }
private:
    std::wstring m_device_name;
    std::wstring m_map_to_drive;
};


// Wrapper around VSS_SNAPSHOT_PROP that provides nicer member access and
//  deals with memory management
class VssSnapshotProperties {
public:
    VssSnapshotProperties(IVssBackupComponents* vss, VSS_ID snapshot_id) {
        CHECKED_CALL(vss->GetSnapshotProperties(snapshot_id, &m_properties), L"GetSnapshotProperties");
    }
    ~VssSnapshotProperties() {
        VssFreeSnapshotProperties(&m_properties);
    }
    const VSS_ID& SnapshotId() const {return m_properties.m_SnapshotId;}
    const VSS_ID& SnapshotSetId() const {return m_properties.m_SnapshotSetId;}
    LONG SnapshotsCount() const {return m_properties.m_lSnapshotsCount;}
    std::wstring SnapshotDeviceObject() const {return str(m_properties.m_pwszSnapshotDeviceObject);}
    std::wstring OriginalVolumeName() const {return str(m_properties.m_pwszOriginalVolumeName);}
    std::wstring OriginatingMachine() const {return str(m_properties.m_pwszOriginatingMachine);}
    std::wstring ServiceMachine() const {return str(m_properties.m_pwszServiceMachine);}
    std::wstring ExposedName() const {return str(m_properties.m_pwszExposedName);}
    std::wstring ExposedPath() const {return str(m_properties.m_pwszExposedPath);}
    const VSS_ID& ProviderId() const {return m_properties.m_ProviderId;}
    LONG SnapshotAttributes() const {return m_properties.m_lSnapshotAttributes;}
    VSS_TIMESTAMP CreationTimestamp() const {return m_properties.m_tsCreationTimestamp;}
    VSS_SNAPSHOT_STATE Status() const {return m_properties.m_eStatus;}
private:
    static std::wstring str(VSS_PWSZ s) {
        if (s)
            return s;
        return std::wstring();
    }
    VSS_SNAPSHOT_PROP m_properties;
};


class VssSnapshot {
public:
    VssSnapshot(IVssBackupComponents* xvss, const std::wstring& volume_name) : m_vss(xvss) {
        CHECKED_CALL(m_vss->InitializeForBackup(), L"InitializeForBackup");

        // Set various properties per backup components instance
        CHECKED_CALL(m_vss->SetBackupState(true, true, VSS_BT_FULL, false), L"SetBackupState");

        {
            CComPtr<IVssAsync> pAsync;
            CHECKED_CALL(m_vss->GatherWriterMetadata(&pAsync), L"GatherWriterMetadata");
            CHECKED_CALL(pAsync->Wait(), L"Wait");
        }

        // Start the snapshot set
        CHECKED_CALL(m_vss->StartSnapshotSet(&m_snapshot_set_id), L"StartSnapshotSet");

        // Only adding the one volume to the snapshot set
        CHECKED_CALL(m_vss->AddToSnapshotSet((LPWSTR)volume_name.c_str(), GUID_NULL, &m_snapshot_id), L"AddToSnapshotSet");

        {
            CComPtr<IVssAsync> pAsync;
            CHECKED_CALL(m_vss->PrepareForBackup(&pAsync), L"PrepareForBackup");
            CHECKED_CALL(pAsync->Wait(), L"Wait");
        }

        {
            CComPtr<IVssAsync>  pAsync;
            CHECKED_CALL(m_vss->DoSnapshotSet(&pAsync), L"DoSnapshotSet");
            CHECKED_CALL(pAsync->Wait(), L"Wait");
        }
    }

    // This is verbose because we're catching errors. Don't want to
    //  throw exceptions in a destructor.
    ~VssSnapshot() {
        {
            CComPtr<IVssAsync>  pAsync;
            if (SUCCEEDED(m_vss->BackupComplete(&pAsync))) {
                if (SUCCEEDED(pAsync->Wait())) {
                    // And delete the snapshot set
                    LONG num_deleted;
                    VSS_ID error_id;
                    if (SUCCEEDED(m_vss->DeleteSnapshots(m_snapshot_set_id, VSS_OBJECT_SNAPSHOT_SET,
                        TRUE, &num_deleted, &error_id))) {
                    }
                }
            }
        }
    }

    VSS_ID snapshot_id() const {return m_snapshot_id;}
    VSS_ID snapshot_set_id() const {return m_snapshot_set_id;}
private:
    IVssBackupComponents*   m_vss;
    VSS_ID m_snapshot_set_id;
    VSS_ID m_snapshot_id;
};


static std::wstring
get_snapshot_device_from_id(IVssBackupComponents* vss, VSS_ID snapshot_id)
{
    // returns a name like: \\?\GLOBALROOT\Device\HarddiskVolumeShadowCopy30
    VssSnapshotProperties properties(vss, snapshot_id);
    return properties.SnapshotDeviceObject();
}


static std::wstring
quote_single_param(const std::wstring& str)
{
    // How to quote effectively? How to escape embedded quotes?
    //  What about spaces? For calling cygwin rsync with no quotes
    //  in the parameter, this works. It works with embedded spaces.
    static std::wstring quote = L"\'";
    return quote + str + quote;
}


static DWORD
execute(std::wstring command, const std::vector<std::wstring> args)
{
    static std::wstring space = L" ";

    STARTUPINFO si;
    memset(&si, 0, sizeof(si));
    si.cb = sizeof(si);

    PROCESS_INFORMATION pi;
    memset(&pi, 0, sizeof(pi));

    // Build up the entire command line, starting with the command
    std::wstring cmdline = quote_single_param(command);

    // Append the arguments, quoting them as we go
    for (std::vector<std::wstring>::const_iterator arg = args.begin(); arg != args.end(); ++arg) {
        // This is an inefficient way to build up a string, but it's not a huge deal
        cmdline += space + quote_single_param(*arg);
    }

    logger.log(boost::wformat(L"executing: '%s'") % cmdline);

    // Copy cmdline into a writable buffer, required by CreateProcess's second argument
    CAutoVectorPtr<WCHAR>    cmdline_buf;
    cmdline_buf.Allocate(cmdline.length()+1);
    wcscpy_s(cmdline_buf, cmdline.length()+1, cmdline.c_str());

    // Start the child process.
    CHECKED_WIN32_CALL(CreateProcess(command.c_str(),
                                     cmdline_buf,             // Entire command line. Might be written to
                                     NULL,                    // Process handle not inheritable
                                     NULL,                    // Thread handle not inheritable
                                     TRUE,                    // Inherit file handles. Needed for pipelining input/output
                                     0,                       // Creation flags
                                     NULL,                    // Use parent's environment block
                                     NULL,                    // Use parent's starting directory
                                     &si,                     // Our STARTUPINFO
                                     &pi),                    // Our PROCESS_INFORMATION
                       L"CreateProcess");

    // Close the process and thread handles when we exit this function
    CHandle handleProcess(pi.hProcess);
    CHandle handleThread(pi.hThread);

    // Wait for the child to exit
    CHECKED_WIN32_CALL(WaitForSingleObject(pi.hProcess, INFINITE) == WAIT_OBJECT_0, L"WaitForSingleObject");

    // Retrieve the exit code
    DWORD dwExitCode;
    CHECKED_WIN32_CALL(GetExitCodeProcess(pi.hProcess, &dwExitCode), L"GetExitCodeProcess");
    logger.log(boost::wformat(L"process exits with %d") % dwExitCode);
    return dwExitCode;
}


static void
print_usage(const std::wstring& argv0, const std::wstring& error)
{
    fprintf(stderr, "%ls", (boost::wformat(L"\nusage: %1% [<arbitrary args>]* <path-looking-arg>\n"
                                           L" <arbitrary-args> will be passed to the executed command.\n"
                                           L" <path-looking-arg> must be of the form:\n"
                                           L"   \"/\" [key=value \"|\"]* DOS-path\n"
                                           L"     keys can be:\n"
                                           L"       tmp-drive    A single letter, which is the drive letter the snapshot will\n"
                                           L"                      be mapped to.\n"
                                           L"       rsync        The native (Windows) path name to the command to execute.\n"
                                           L"       cygpath      If present and 'true', then the final path argument passed\n"
                                           L"                      to the command will be in cygwin format, otherwise it will\n"
                                           L"                      be in DOS format.\n"
                                           L"       logfile      If present, a log file of this name will be written to.\n"
                                           L"       verbose      If present and 'true', verbose messages will be written\n"
                                           L"                      to stderr and the log file.\n"
                                           L"       rsync-arg-0,\n"
                                           L"       rsync-arg-1,\n"
                                           L"        etc         If present, theses arguments are added to the command before\n"
                                           L"                      execution. The original use is to allow --fake-super to be\n"
                                           L"                      passed to rsync.\n"
                                           L"     DOS-path will be passed to the command as the final parameter, and is used\n"
                                           L"       to determine the drive to take a snapshot of.\n"
                                           L"example: %1% \"/|tmp-drive=x|rsync=c:\\windows\\system32\\cmd.exe|cygpath=false|verbose=true|c:/users\"\n"
                                           L"\n%2%\n\n") % argv0 % error).str().c_str());
}


#define SEPARATOR L'|'

static bool
is_separator(wchar_t ch)
{
    return ch == SEPARATOR;
}


// split strings of the form: '/|key0=value0|key1=value1|key2=value2|/rest/of/the/path'
// returns a 2-tuple: dict with key=value pairs, and the entire rest of the path
// so for the example, it returns ({'key0':'value0','key1':'value1','key2':'value2'},'/rest/of/the/path')
static void
split_final_args(const std::wstring& str,
                 std::map<std::wstring, std::wstring>& params,
                 std::wstring& real_path)
{
    // find the first and last vertical bars
    size_t first = str.find(SEPARATOR);
    size_t last  = str.rfind(SEPARATOR);

    if (first == str.npos || last == str.npos) {
        throw CError(0, boost::wformat(L"No delimiters found in '%s'") % str);
    }

    // find the substring, skipping the initial and final '|'
    std::wstring key_values = str.substr(first+1, last-first-1);

    real_path = str.substr(last+1);

    // key_values now looks like '|key0=value0|key1=value1|key2=value2'

    std::vector<std::wstring> key_value_strs;
    boost::split(key_value_strs, key_values, is_separator);

    // now for each key/value pair, split on '='
    for (size_t i = 0; i < key_value_strs.size(); i++) {
        std::wstring& kv = key_value_strs[i];
        size_t pos = kv.find(L'=');
        if (pos == kv.npos) {
            // missing =
            throw CError(0, boost::wformat(L"No equal sign found in '%s'") % kv);
        }
        params[kv.substr(0, pos)] = kv.substr(pos+1);
    }
}


// rsync_args will contain all of the arguments to rsync
static void
get_params(int argc, wchar_t** argv, wchar_t& source_drive_letter, wchar_t& tmp_drive_letter, std::wstring& rsync_command, std::vector<std::wstring>& rsync_args)
{
    // Very first thing, even before parsing the commands (including
    // figuring out if we're in verbose or file-logging mode, is
    // log our arguments. They'll end up on stderr.
    for (int i = 0; i < argc; i++) {
        logger.log(boost::wformat(L"%d: %s") % i % argv[i]);
    }
    // Must have at least 1 arg
    if (argc <= 1) {
        throw CError(0, boost::wformat(L"At least 1 parameter required, %d supplied") % (argc-1));
    }

    // copy all of the normal rsync_args (all except the final one, which is treated specially below)
    rsync_args.insert(rsync_args.end(), argv+1,  argv+argc-1);

    // from the last arg, extract the various key=value arguments, plus the real rsync path
    std::wstring real_path;
    std::map<std::wstring, std::wstring> params;
    split_final_args(argv[argc-1], params, real_path);

    // first, see if we're writing a log file. if this value isn't set, it's a zero length
    // string, which just means don't write to a log file
    std::wstring log_filename = params[L"logfile"];
    logger.set_filename(log_filename);
    logger.log(boost::wformat(L"startup"));

    std::wstring verbose = params[L"verbose"];
    if (verbose == L"true") {
        logger.set_verbose(true);
    }

    // extract the tmp drive letter
    std::wstring tmp_drive = params[L"tmp-drive"];
    if (tmp_drive.length() != 1) {
        throw CError(0, L"tmp-drive not of length 1");
    }
    tmp_drive_letter = tmp_drive[0];
    logger.debug(boost::wformat(L"tmp_drive_letter: %lc") % tmp_drive_letter);

    // get the rsync command and validate it
    rsync_command = params[L"rsync"];
    if (rsync_command.length() == 0) {
        throw CError(0, L"no rsync command specified");
    }
    logger.debug(boost::wformat(L"rsync_command: %ls") % rsync_command);

    // save the real rsync path as the last rsync argument, except change the drive letter
    // real_path must begin a drive letter, colon, and slash (or backslash)
    logger.debug(boost::wformat(L"original real_path: '%ls'") % real_path);
    if (real_path.length() < 3) {
        throw CError(0, L"real path length too short");
    }
    if (real_path[1] != L':' || !(real_path[2] == L'/' || real_path[2] == L'\\')) {
        throw CError(0, L"real path must start with '<drive-letter>:/'");
    }

    // save the source drive letter, used by the caller to create a volume shadow copy
    source_drive_letter = real_path[0];

    // build up the real path to pass to rsync
    if (params[L"cygpath"] == L"true") {
        // the callee is a cygwin program, munge the path to be /cygdrive based
        real_path = (boost::wformat(L"/cygdrive/%1%%2%") % tmp_drive_letter % real_path.substr(2)).str();
    } else {
        // just substitute the drive letter
        real_path[0] = tmp_drive_letter;
    }
    logger.debug(boost::wformat(L"modified real_path: %ls") % real_path);
    // and stick it on the end of the rsync args
    rsync_args.push_back(real_path);

    // look for args named "rsync-arg-0", "rsync-arg-1", etc.
    // if found, insert then at the front of the rsync args
    for (unsigned idx = 0; ; ++idx) {
        std::wstring arg_name = (boost::wformat(L"rsync-arg-%1%") % idx).str();

        std::wstring arg_value = params[arg_name];

        if (arg_value.length() == 0) {
            // no arg (or zero length), get out
            break;
        }

        // insert this at the front, in order
        rsync_args.insert(rsync_args.begin()+idx, arg_value);
    }
}


// Return just the part of argv[0] after the final slash or backslash, if any
// Could also strip off .exe, if present, but doesn't seem worth the hassle
static std::wstring
get_argv0(std::wstring argv0)
{
    size_t idx = argv0.find_last_of(L"/\\");
    if (idx != std::wstring::npos) {
        argv0 = argv0.substr(idx+1);
    }
    return argv0;
}


static DWORD
shadow_map_drive_and_execute(wchar_t source_drive_letter, wchar_t temp_drive_letter, const std::wstring& command, const std::vector<std::wstring>& args)
{
    // source_drive_letter is the drive letter we're going to be snapshotting. probably 'c'
    // temp_drive_letter is where we're going to mount the snapshot. something like 'x'
    // command is what we're going to execute after we've shadowed and mapped the drive
    // args are arguments passed to command

    // The snapshot volume name has to look like "c:\"
    std::wstring snapshot_volume_name(&source_drive_letter, 1);
    snapshot_volume_name += L":\\";

    // Create our connection to VSS
    CComPtr<IVssBackupComponents>   vss;
    CHECKED_CALL(CreateVssBackupComponents(&vss), L"CreateVssBackupComponents");

    // Create the snapshot
    VssSnapshot snapshot(vss, snapshot_volume_name);

    // Map the drive to a drive letter, which we pass to the command
    DriveMapper mapper(get_snapshot_device_from_id(vss, snapshot.snapshot_id()), temp_drive_letter);

    // Execute the command, return its result as ours
    return execute(command, args);
 }


int _tmain(int argc, WCHAR* argv[])
{
    wchar_t source_drive_letter;
    wchar_t tmp_drive_letter;
    std::wstring rsync_command;
    std::vector<std::wstring> rsync_args;

    try {
        // Collect and validate the params
        get_params(argc, argv, source_drive_letter, tmp_drive_letter, rsync_command, rsync_args);
    } catch (const CError& exc) {
        print_usage(get_argv0(argv[0]), exc.m_str);
        return -1;
    }

    try {

        // Initialize COM
        ComInitialize   init;
        ComSecurity     sec;

        // Do the work
        return shadow_map_drive_and_execute(source_drive_letter, tmp_drive_letter, rsync_command, rsync_args);

    } catch (const CAtlException& exc) {
        // Log the error and return
        logger.log(boost::wformat(L"fatal error: %#x") % exc.m_hr);
        return 7;

    } catch (const CError& exc) {
        logger.log(boost::wformat(L"fatal error: %#x in %s %s") % exc.m_hr % exc.m_str.c_str() % exc.m_line);
        return 8;
    }
}
